﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using webapi.Infrastructure;
using webapi.Middleware;
using webapi.Services;

namespace webapi
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        private const string CORS_POLICY = "SOME_CORS_POLICY";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IGeodesicDistanceCalculator, GeodesicDistanceCalculator>();
            services.AddIDistanceCalculatorFunctionFactory();

            services.AddCors(o =>
            {
                o.AddPolicy(CORS_POLICY,
                    b =>
                    {
                        b.AllowAnyHeader();
                        b.AllowAnyMethod();
                        b.WithOrigins(Configuration.GetSection("Cors:AllowedUrls")?.Get<string[]>() ?? new[] { "*" });
                        b.SetPreflightMaxAge(TimeSpan.FromSeconds(3600));
                    });
            });

            services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new Info { Title = "Distance API", Version = "v1" });
                });
            services.Configure<RouteOptions>(o =>
            {
                o.ConstraintMap.Add("doubleRange", typeof(DoubleRangeRouteConstraint));
            });
            services.AddResponseCompression();

            services.Configure<RequestLocalizationOptions>(o =>
            {
                var allowedCultures = new[]
                {
                    "en-IE",
                    "en-GB",
                    "en-US",
                    "en",
                };
                o.DefaultRequestCulture = new RequestCulture("en-IE");
                o.RequestCultureProviders.Clear();
                o.RequestCultureProviders.Add(new AcceptLanguageHeaderRequestCultureProvider());
                o.AddSupportedCultures(allowedCultures).AddSupportedUICultures(allowedCultures);
            });

            services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();
            app.UseRequestLocalization();

            app.UseSwagger();
            if (!env.IsProduction())
            {
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Distance API V1");
                });
            }
            app.UseCors(CORS_POLICY);
            app.UseResponseCompression();
            app.UseMiddleware<ExceptionHandlingMiddleware>();
            app.UseMvc();
        }
    }
}