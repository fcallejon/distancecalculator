using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace webapi.Middleware
{
    /// <summary>
    /// Quick and dirt middleware handler for exceptions
    /// </summary>
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IReadOnlyDictionary<Type, Func<Exception, HttpStatusCode>> _codesByType;

        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
            _codesByType = new Dictionary<Type, Func<Exception, HttpStatusCode>>
            {
                { typeof(ArgumentException), e => HttpStatusCode.BadRequest },
                { typeof(ArgumentNullException), e => HttpStatusCode.BadRequest },
                { typeof(ArgumentOutOfRangeException), e => HttpStatusCode.BadRequest }
            };
        }

        public async Task Invoke(HttpContext context, ILogger<ExceptionHandlingMiddleware> logger)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, ex.Message);
                await HandleExceptionAsync(context, ex);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var code = HttpStatusCode.InternalServerError;
            if (_codesByType.TryGetValue(exception.GetType(), out var getCode))
            {
                code = getCode(exception);
            }

            context.Response.StatusCode = (int)code;
            context.Response.ContentType = "text/plain";
            return context.Response.WriteAsync(exception.Message);
        }
    }
}