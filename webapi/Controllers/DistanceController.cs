﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using webapi.Services;
using webapi.Models;

namespace webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DistanceController : ControllerBase
    {
        private readonly IDistanceCalculator _distanceCalculator;

        public DistanceController(Func<IDistanceCalculator> distanceCalculatorFactory)
        {
            _distanceCalculator = distanceCalculatorFactory();
        }

        /// <summary>
        /// Returns the distance between two points on Earth.
        /// </summary>
        /// <param name="fromLatitude">Latitude for the From City.</param>
        /// <param name="fromLongitude">Longitude for the From City.</param>
        /// <param name="toLatitude">Latitude for the To City.</param>
        /// <param name="toLongitude">Longitude for the To City.</param>
        /// <remarks>Use the language header to change the units. EN-GB, for some reason, is a meter region.</remarks>
        /// <returns>A <see cref="DistanceResult" /> containing the result.</returns>
        [HttpGet]
        [Route("{fromLatitude:doubleRange(-179,179)},{fromLongitude:doubleRange(-89,89)},{toLatitude:doubleRange(-179,179)},{toLongitude:doubleRange(-89,89)}")]
        public ActionResult<DistanceResult> Get(
            double fromLatitude, double fromLongitude,
            double toLatitude, double toLongitude)
        {            
            var from = new Position
            {
                Latitude = fromLatitude,
                Longitude = fromLongitude
            };
            var to = new Position
            {
                Latitude = toLatitude,
                Longitude = toLongitude
            };
            return Ok(_distanceCalculator.Calculate(from, to));
        }
    }
}
