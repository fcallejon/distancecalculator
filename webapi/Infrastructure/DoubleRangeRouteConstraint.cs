using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace webapi.Infrastructure
{
    /// <summary>
    /// Class to define an <see cref="IRouteConstraint" /> to handle dobule values in a range.
    /// </summary>
    public class DoubleRangeRouteConstraint : IRouteConstraint
    {
        private readonly double _minimum;
        private readonly double _maximum;

        public DoubleRangeRouteConstraint(double minimum, double maximum)
        {
            _minimum = minimum;
            _maximum = maximum;
        }

        public bool Match(HttpContext httpContext, IRouter route, string routeKey, RouteValueDictionary values, RouteDirection routeDirection)
        {
            if (routeKey == null)
            {
                throw new ArgumentNullException(nameof(routeKey));
            }

            if (values == null)
            {
                throw new ArgumentNullException(nameof(values));
            }
            
            var value = values[routeKey];
            if (value == null || !double.TryParse(value.ToString(), out var asDouble))
            {
                return false;
            }
            return asDouble >= _minimum && asDouble <= _maximum;
        }
    }
}