using System;
using System.Diagnostics.CodeAnalysis;

namespace webapi.Extensions
{
    [ExcludeFromCodeCoverage]
    /// <summary>
    /// Methods to convert values between Radians and Degrees
    /// </summary>
    public static class DoubleExtensions
    {
        /// <summary>
        /// Convert degrees to radians.
        /// </summary>
        /// <param name="degree">The value to convert.</param>
        public static double ToRadians(this double degree)
        {
            return degree * Math.PI / 180.0;
        }

        /// <summary>
        /// Converto radians to degrees.
        /// </summary>
        /// <param name="radians">The value to convert.</param>
        public static double ToDegrees(this double radians)
        {
            return radians / Math.PI * 180.0;
        }
    }
}