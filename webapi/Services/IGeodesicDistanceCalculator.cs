using webapi.Models;

namespace webapi.Services
{
    public interface IGeodesicDistanceCalculator
    {
         double CalculateDistance(Position from, Position to);
    }
}