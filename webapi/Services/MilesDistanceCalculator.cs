using webapi.Models;

namespace webapi.Services
{
    /// <summary>
    /// Class to calculate the distance in miles
    /// </summary>
    public class MilesDistanceCalculator : IDistanceCalculator
    {
        public const double EARTH_MEAN_RADIOUS_MILES = 3958.761e3;
        private readonly IGeodesicDistanceCalculator _calculator;

        public MilesDistanceCalculator(IGeodesicDistanceCalculator calculator)
        {
            _calculator = calculator;
        }

        public DistanceResult Calculate(Position from, Position to)
        {
            if (from == to)
            {
                return new DistanceResult
                {
                    Unit = "Km",
                    Distance = 0
                };
            }
            var distance = EARTH_MEAN_RADIOUS_MILES * _calculator.CalculateDistance(from, to) / 1000;
            return new DistanceResult
            {
                Unit = "Mi",
                Distance = distance
            };
        }
    }
}