using webapi.Models;

namespace webapi.Services
{
    /// <summary>
    /// Class to calculate the distance in kilometers
    /// </summary>
    public class KilometersDistanceCalculator : IDistanceCalculator
    {
        public const double EARTH_MEAN_RADIOUS_METERS = 6371e3;
        private readonly IGeodesicDistanceCalculator _calculator;

        public KilometersDistanceCalculator(IGeodesicDistanceCalculator calculator)
        {
            _calculator = calculator;
        }

        public DistanceResult Calculate(Position from, Position to)
        {
            if (from == to)
            {
                return new DistanceResult
                {
                    Unit = "Km",
                    Distance = 0
                };
            }
            var distance = EARTH_MEAN_RADIOUS_METERS * _calculator.CalculateDistance(from, to) / 1000;
            return new DistanceResult
            {
                Unit = "Km",
                Distance = distance
            };
        }
    }
}