using Microsoft.AspNetCore.Mvc;
using webapi.Models;

namespace webapi.Services
{
    /// <summary>
    /// Class to calculate the distance from PositionA to PositionB
    /// </summary>
    public interface IDistanceCalculator
    {
        /// <summary>
        /// Calculates a distance between two points.
        /// </summary>
        /// <param name="from">A <see cref="Position" /> in Earth.</param>
        /// <param name="to">A <see cref="Position" /> in Earth.</param>
        /// <returns>A <see cref="DistanceResult" /> between the positions.</returns>
        DistanceResult Calculate(Position from, Position to);
    }
}