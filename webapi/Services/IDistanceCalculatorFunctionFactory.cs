using System;
using System.Globalization;
using Microsoft.Extensions.DependencyInjection;

namespace webapi.Services
{
    /// <summary>
    /// Fast&Furious Factory function
    /// </summary>
    public static class IDistanceCalculatorFunctionFactory
    {
        public static void AddIDistanceCalculatorFunctionFactory(this IServiceCollection services)
        {
            services.AddScoped<Func<IDistanceCalculator>>(ctx =>
                    () =>
                        new RegionInfo(CultureInfo.CurrentUICulture.LCID).IsMetric
                        ? new KilometersDistanceCalculator(ctx.GetService<IGeodesicDistanceCalculator>()) as IDistanceCalculator
                        : new MilesDistanceCalculator(ctx.GetService<IGeodesicDistanceCalculator>()));
        }
    }
}