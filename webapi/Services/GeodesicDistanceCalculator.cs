using System;
using System.Globalization;
using webapi.Extensions;
using webapi.Models;

namespace webapi.Services
{
    /// <summary>
    /// Class to calculate a distance between to points using Geodesic Distance
    /// </summary>
    public class GeodesicDistanceCalculator : IGeodesicDistanceCalculator
    {
        /// <summary>
        /// Calculates a distance between two points.
        /// </summary>
        /// <param name="from">A <see cref="Position" /> in Earth.</param>
        /// <param name="to">A <see cref="Position" /> in Earth.</param>
        /// <returns>A <see cref="DistanceResult" /> between the positions.</returns>
        public double CalculateDistance(Position from, Position to)
        {
            if (from == to)
            {
                return 0;
            }
            var theta = (from.Longitude - to.Longitude).ToRadians();
            var rawDistance = Math.Sin(from.Latitude.ToRadians()) * Math.Sin(to.Latitude.ToRadians()) +
                            Math.Cos(from.Latitude.ToRadians()) * Math.Cos(to.Latitude.ToRadians()) * Math.Cos(theta);
            rawDistance = Math.Acos(rawDistance);

            return rawDistance;
        }
    }
}