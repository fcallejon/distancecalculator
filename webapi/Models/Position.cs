using System;

namespace webapi.Models
{
    public class Position
    {
        /// <summary>
        /// A Latitude value in degrees
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// A Longitude value in degrees
        /// </summary>
        public double Longitude { get; set; }

        public override int GetHashCode()
        {
            return (int)Latitude ^ 17 +
                    (int)Longitude ^ 173;
        }

        public override bool Equals(object obj)
        {
            var otherPosition = obj as Position;
            if (object.ReferenceEquals(otherPosition, null))
            {
                return false;
            }

            return GetHashCode() == otherPosition.GetHashCode();
        }

        public static bool operator !=(Position left, Position right)
        {
            return 
                !(object.ReferenceEquals(left, null) && object.ReferenceEquals(right, null))
                && !left.Equals(right);
        }

        public static bool operator ==(Position left, Position right)
        {
            return 
                (object.ReferenceEquals(left, null) && object.ReferenceEquals(right, null))
                || left.Equals(right);
        }
    }
}