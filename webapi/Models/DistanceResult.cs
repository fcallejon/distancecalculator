using System.Diagnostics.CodeAnalysis;
using System.Globalization;

namespace webapi.Models
{
    [ExcludeFromCodeCoverage]
    public class DistanceResult
    {
        public string Unit { get; set; }
        public double Distance { get; set; }
    }
}