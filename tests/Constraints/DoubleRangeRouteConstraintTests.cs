using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using webapi.Infrastructure;

namespace tests.Constraints
{
    [TestClass]
    public class DoubleRangeRouteConstraintTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Match_WhenRouteKeyNull_Throw()
        {
            var constraint = new DoubleRangeRouteConstraint(0, 100);
            var httpContext = new DefaultHttpContext();
            constraint.Match(httpContext, null, "route", null, RouteDirection.IncomingRequest);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Match_WhenValuesNull_Throw()
        {
            var constraint = new DoubleRangeRouteConstraint(0, 100);
            var httpContext = new DefaultHttpContext();
            constraint.Match(httpContext, null, null, null, RouteDirection.IncomingRequest);
        }

        [TestMethod]
        public void Match_WhenNoValue_ReturnFalse()
        {
            var constraint = new DoubleRangeRouteConstraint(0, 100);
            var httpContext = new DefaultHttpContext();
            var values = new RouteValueDictionary();
            var routeKey = "test";
            values.Add(routeKey, null);
            constraint.Match(httpContext, null, routeKey, values, RouteDirection.IncomingRequest);
        }

        [TestMethod]
        public void Match_WhenValueLessThanMin_ReturnFalse()
        {
            var constraint = new DoubleRangeRouteConstraint(0, 100);
            var httpContext = new DefaultHttpContext();
            var values = new RouteValueDictionary();
            var routeKey = "test";
            values.Add(routeKey, -5);
            constraint.Match(httpContext, null, routeKey, values, RouteDirection.IncomingRequest);
        }

        [TestMethod]
        public void Match_WhenValueGreaterThanMax_ReturnFalse()
        {
            var constraint = new DoubleRangeRouteConstraint(0, 100);
            var httpContext = new DefaultHttpContext();
            var values = new RouteValueDictionary();
            var routeKey = "test";
            values.Add(routeKey, 105);
            constraint.Match(httpContext, null, routeKey, values, RouteDirection.IncomingRequest);
        }

        [TestMethod]
        public void Match_WhenValueValid_ReturnTrue()
        {
            var constraint = new DoubleRangeRouteConstraint(0, 100);
            var httpContext = new DefaultHttpContext();
            var values = new RouteValueDictionary();
            var routeKey = "test";
            values.Add(routeKey, 50);
            constraint.Match(httpContext, null, routeKey, values, RouteDirection.IncomingRequest);
        }
    }
}