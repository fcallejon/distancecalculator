using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using webapi.Controllers;
using webapi.Models;
using webapi.Services;

namespace tests.Controllers
{
    [TestClass]
    public class DistanceControllerTests
    {
        [TestMethod]
        public void Get_WhenPositionsOk_ReturnValidResult()
        {
            var expectedDistance = 10;
            var expectedUnit = "unit";
            var distanceCalculator = new Mock<IDistanceCalculator>();
            distanceCalculator
                .Setup(dc => dc.Calculate(It.IsAny<Position>(), It.IsAny<Position>()))
                .Returns(new DistanceResult
                {
                    Distance = expectedDistance,
                    Unit = expectedUnit
                });
            var controller = new DistanceController(() => distanceCalculator.Object);

            var result = controller.Get(10, 10, 10, 10).Result as OkObjectResult;
            var distanceResult = result.Value as DistanceResult;
            Assert.AreEqual(expectedDistance, distanceResult.Distance);
            Assert.AreEqual(expectedUnit, distanceResult.Unit);
        }
    }
}