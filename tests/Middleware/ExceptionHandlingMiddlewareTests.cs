using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using webapi.Middleware;

namespace tests.Middleware
{
    [TestClass]
    public class ExceptionHandlingMiddlewareTests
    {
        [TestMethod]
        public async Task InvokeHandleExceptionAsync_WhenNotInCodeTypes_Return500()
        {
            var logger = new Mock<ILogger<ExceptionHandlingMiddleware>>();

            var middleware = new ExceptionHandlingMiddleware(RequestDelegateMethodException);
            var httpContext = new DefaultHttpContext();
            await middleware.Invoke(httpContext, logger.Object);

            Assert.AreEqual(500, httpContext.Response.StatusCode);

            Task RequestDelegateMethodException(HttpContext context)
            {
                return Task.FromException(new Exception());
            }
        }

        [TestMethod]
        public async Task InvokeHandleExceptionAsync_WhenInCodeTypes_Return400()
        {
            var logger = new Mock<ILogger<ExceptionHandlingMiddleware>>();

            var middleware = new ExceptionHandlingMiddleware(RequestDelegateMethodArgumentException);
            var httpContext = new DefaultHttpContext();
            await middleware.Invoke(httpContext, logger.Object);

            Assert.AreEqual(400, httpContext.Response.StatusCode);            

            Task RequestDelegateMethodArgumentException(HttpContext context)
            {
                return Task.FromException(new ArgumentException());
            }
        }
    }
}