using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using webapi.Services;

namespace tests.Services
{
    [TestClass]
    public class IDistanceCalculatorFactoryTests
    {
        [TestMethod]
        public void GetIDistanceCalculatorFunc_WhenIsMetricTrue_ReturnsKilometersDistanceCalculator()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<IGeodesicDistanceCalculator, GeodesicDistanceCalculator>();
            serviceCollection.AddIDistanceCalculatorFunctionFactory();

            var descriptor = serviceCollection
                            .First(s => s.ImplementationFactory?.Method?.ReturnType == typeof(Func<IDistanceCalculator>));

            var irelandCulture = new CultureInfo("en-IE");
            Thread.CurrentThread.CurrentUICulture = irelandCulture;
            Thread.CurrentThread.CurrentCulture = irelandCulture;

            var factory = descriptor.ImplementationFactory
                                .Method
                                .Invoke(descriptor.ImplementationFactory.Target, new [] { serviceCollection.BuildServiceProvider() })
                            as Func<IDistanceCalculator>;
            var calculator = factory();
            Assert.IsInstanceOfType(calculator, typeof(KilometersDistanceCalculator));
        }
        
        [TestMethod]
        public void GetIDistanceCalculatorFunc_WhenIsMetricFalse_ReturnsMilesDistanceCalculator()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<IGeodesicDistanceCalculator, GeodesicDistanceCalculator>();
            serviceCollection.AddIDistanceCalculatorFunctionFactory();

            var descriptor = serviceCollection
                            .First(s => s.ImplementationFactory?.Method?.ReturnType == typeof(Func<IDistanceCalculator>));

            var irelandCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = irelandCulture;
            Thread.CurrentThread.CurrentCulture = irelandCulture;

            var factory = descriptor.ImplementationFactory
                                .Method
                                .Invoke(descriptor.ImplementationFactory.Target, new [] { serviceCollection.BuildServiceProvider() })
                            as Func<IDistanceCalculator>;
            var calculator = factory();
            Assert.IsInstanceOfType(calculator, typeof(MilesDistanceCalculator));
        }
    }
}