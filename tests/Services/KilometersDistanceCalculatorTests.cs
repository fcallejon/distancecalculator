using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using webapi.Models;
using webapi.Services;

namespace tests.Services
{
    [TestClass]
    public class KilometersDistanceCalculatorTests
    {
        [TestMethod]
        public void Calculate_WhenDifferentPoints_ShouldCallGeodesic()
        {
            var expected = KilometersDistanceCalculator.EARTH_MEAN_RADIOUS_METERS / 1000;
            var from = new Position
            {
                Latitude = 10,
                Longitude = 20
            };

            var to = new Position
            {
                Latitude = 20,
                Longitude = 10
            };

            var geodesicMock = new Mock<IGeodesicDistanceCalculator>();
            geodesicMock
                .Setup(g => g.CalculateDistance(It.IsAny<Position>(), It.IsAny<Position>()))
                .Returns(1);

            var calculator = new KilometersDistanceCalculator(geodesicMock.Object);
            var result = calculator.Calculate(from, to);

            geodesicMock
                .Verify(g => g.CalculateDistance(It.IsAny<Position>(), It.IsAny<Position>()), Times.Once());
            Assert.AreEqual(expected, result.Distance);
        }
        
        [TestMethod]
        public void Calculate_WhenSamePoints_ShouldNotCallGeodesic()
        {
            var from = new Position
            {
                Latitude = 10,
                Longitude = 20
            };

            var to = new Position
            {
                Latitude = 10,
                Longitude = 20
            };

            var geodesicMock = new Mock<IGeodesicDistanceCalculator>();
            geodesicMock
                .Setup(g => g.CalculateDistance(It.IsAny<Position>(), It.IsAny<Position>()))
                .Returns(1);

            var calculator = new KilometersDistanceCalculator(geodesicMock.Object);
            var result = calculator.Calculate(from, to);

            geodesicMock
                .Verify(g => g.CalculateDistance(It.IsAny<Position>(), It.IsAny<Position>()), Times.Never());
            Assert.AreEqual(0, result.Distance);
        }
    }
}