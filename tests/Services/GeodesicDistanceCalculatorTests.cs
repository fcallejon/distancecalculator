using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using webapi.Models;
using webapi.Services;

namespace tests.Services
{
    [TestClass]
    public class GeodesicDistanceCalculatorTests
    {
        [TestMethod]
        public void CalculateDistance_WhenDifferentPoints_ShouldCallGeodesic()
        {
            var expected = 0.24;
            var calculator = new GeodesicDistanceCalculator();
            var from = new Position
            {
                Latitude = 10,
                Longitude = 20
            };

            var to = new Position
            {
                Latitude = 20,
                Longitude = 10
            };
            var result = Math.Round(calculator.CalculateDistance(from, to), 2);
            Assert.AreEqual(expected, result);
        }
        
        [TestMethod]
        public void CalculateDistance_WhenSamePoints_ShouldNotCallGeodesic()
        {
            var expected = 0;
            var calculator = new GeodesicDistanceCalculator();
            var from = new Position
            {
                Latitude = 20,
                Longitude = 10
            };

            var to = new Position
            {
                Latitude = 20,
                Longitude = 10
            };
            var result = calculator.CalculateDistance(from, to);
            Assert.AreEqual(expected, result);
        }
    }
}