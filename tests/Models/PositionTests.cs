using Microsoft.VisualStudio.TestTools.UnitTesting;
using webapi.Models;

namespace tests.Models
{
    [TestClass]
    public class PositionTests
    {
        [TestMethod]
        public void Equals_WhenRightNull_False()
        {
            var left = new Position
            {
                Latitude = 1,
                Longitude = 2
            };
            Assert.IsFalse(left.Equals(null));
        }

        [TestMethod]
        public void EqualOverride_WhenRightNull_False()
        {
            var left = new Position
            {
                Latitude = 1,
                Longitude = 2
            };
            Assert.IsFalse(left == null);
        }
        
        [TestMethod]
        public void NotEquals_WhenRightNull_True()
        {
            var left = new Position
            {
                Latitude = 1,
                Longitude = 2
            };
            Assert.IsTrue(!left.Equals(null));
        }

        [TestMethod]
        public void NotEqualOverride_WhenRightNull_True()
        {
            var left = new Position
            {
                Latitude = 1,
                Longitude = 2
            };
            Assert.IsTrue(left != null);
        }

        [TestMethod]
        public void Equals_WhenRightEqual_True()
        {
            var left = new Position
            {
                Latitude = 1,
                Longitude = 2
            };
            var right = new Position
            {
                Latitude = 1,
                Longitude = 2
            };
            Assert.IsTrue(left.Equals(right));
        }

        [TestMethod]
        public void EqualOverride_WhenRightEqual_True()
        {
            var left = new Position
            {
                Latitude = 1,
                Longitude = 2
            };
            var right = new Position
            {
                Latitude = 1,
                Longitude = 2
            };
            Assert.IsTrue(left == right);
        }
    }
}